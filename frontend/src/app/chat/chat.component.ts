import { stringify } from '@angular/compiler/src/util';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Message } from '../model/message.model';
import { ChatService } from '../service/chatService';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit,OnDestroy {
  msg = new Message();
  messages = [];
  str:any = {meme:true,pic:"vkusno"};
  s:string;
  
  memArr = ["assets/pics/vkusno.jpg","assets/pics/agro.jpg","assets/pics/avokado.jpg","assets/pics/bb.jpg","assets/pics/bonk.jpg",
  "assets/pics/bonk2.jpg","assets/pics/da.jpg","assets/pics/bykanul.jpg","assets/pics/ezhik.jpg","assets/pics/guchi.jpg","assets/pics/hueta.jpg",
  "assets/pics/konch.jpg","assets/pics/lazyass.jpg","assets/pics/ohui.jpg","assets/pics/paketik.jpg","assets/pics/pizda.jpg","assets/pics/pyatniza.jpg",
  "assets/pics/sorry.jpg","assets/pics/svyazali.jpg"];
  private subscriptions: Subscription[] = [];
  constructor(public service: ChatService) { }

  ngOnInit(): void {
      this.s=JSON.stringify(this.str);
    const msgSub = this.service.msgSub.subscribe(value=>{
      this.messages.push(value);
    });
    this.subscriptions.push(msgSub);
  }
  ngOnDestroy(){
    this.subscriptions
    .forEach(s => s.unsubscribe());
  }

  sendMessage(){
    this.msg.recipient = this.service.correspondent.id;
    this.msg.sender = this.service.currentUser.id;
    this.messages.push(this.msg);
    this.service.sendMessage(this.msg);
    this.msg = new Message();
  }
  sendPic(picId){
    if(this.service.correspondent.id != null){
      this.msg.recipient = this.service.correspondent.id;
      this.msg.sender = this.service.currentUser.id;
      this.msg.content = JSON.stringify({pic:picId});
      this.messages.push(this.msg);
      this.service.sendMessage(this.msg);
      this.msg = new Message();
    }
  }
  parse(str:string):any{
    try {
      return JSON.parse(str);
    }catch(exep){
      return '{"pic":null}';
    }
  }
}
