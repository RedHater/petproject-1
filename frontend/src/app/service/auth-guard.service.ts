import { Injectable } from "@angular/core";
import {
  CanActivate,
  Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
import { ChatService } from "./chatService";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(private router: Router, private service:ChatService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if(this.service.currentUser.id == null){
      this.router.navigateByUrl("/sign-in");
    }
    return true;
  }
}
