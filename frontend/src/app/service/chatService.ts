import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "../model/user.model";
import { ReplaySubject } from "rxjs";
import { Message } from "../model/message.model";
import { AcceptRequest, State } from "../model/accept-request.model";
import { MatDialog } from "@angular/material/dialog";
import { AcceptDialogComponent } from "../accept-dialog/accept-dialog.component";
import { ActivatedRoute } from "@angular/router";

declare var SockJS;
declare var Stomp;
@Injectable({
    providedIn: 'root'
})
export class ChatService {
    public msgSub: ReplaySubject<string> = new ReplaySubject<string>();
    public usersSub: ReplaySubject<boolean> = new ReplaySubject<boolean>();
    public currentUser:User = new User();
    public correspondent:User;
    public stompClient;
    constructor(private http: HttpClient,  public dialog: MatDialog,private route:ActivatedRoute) { }

    createAcceptDialog(row:AcceptRequest): void {
        let data = row;
        const dialogRef = this.dialog.open(AcceptDialogComponent, {
          data,
          width: "100%",
          maxWidth: "none",
        });
        dialogRef.afterClosed().subscribe(value=>{
            if(value){
               data.state = State.ACCEPTED;
               this.correspondent = data.sender;
            }else{
                data.state = State.DECLINED;
            }
            this.acceptRequest(data);
        })
      }

    initializeWebSocketConnection() {
        const serverUrl = `http://${window.location.host}/chat`;
        const ws = new SockJS(serverUrl);
        this.stompClient = Stomp.over(ws);
        const that = this;
        this.stompClient.debug = null;
        this.stompClient.connect({}, function (frame) {
            that.stompClient.subscribe("/user/" + that.currentUser.id + "/messages", (message) => {
              if (message.body) {
               that.msgSub.next(JSON.parse(message.body));
              }
            });
            that.stompClient.subscribe("/user/" + that.currentUser.id + "/chatReqs", (req)=>{
                if(req.body){
                    if(JSON.parse(req.body).state == 'SENT'){
                        that.createAcceptDialog(JSON.parse(req.body));
                    }else{
                        that.correspondent = JSON.parse(req.body).recipient;
                    }
                }
            });
            that.stompClient.subscribe("/users/", (req)=>{
                if(req.body){
                    that.usersSub.next(true);
                }
            });
            that.stompClient.send("/app/new",{},JSON.stringify("new"));
        },function(message) {
        });
    }

    sendMessage(message:Message){
        this.stompClient.send("/app/chat",{},JSON.stringify(message));
    }
    sendRequest(request:AcceptRequest){
        this.stompClient.send("/app/req",{},JSON.stringify(request));
    }
    acceptRequest(request:AcceptRequest){
        this.stompClient.send("/app/accept",{},JSON.stringify(request));
    }
    signIn(nickname){
        return this.http.get<any>(`/api/user/signIn/${nickname}`);
    }
    deleteCurrentUser(){
        this.http.get<any>(`/api/user/delete/${this.currentUser.id}`).subscribe();
    }
    getByName(nickname){
        return this.http.get<any>(`/api/user/getByName/${nickname}`);
    }
    getAll(){
        return this.http.get<any[]>(`/api/user/all/`);
    }
}  