import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { AuthGuardService } from "./service/auth-guard.service";
import { SignInComponent } from './sign-in/sign-in.component';
const routes: Routes = [
   {
    path: "",
    component: MainComponent,
    canActivate: [AuthGuardService],
  },
  {
    path: "sign-in",
    component: SignInComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
