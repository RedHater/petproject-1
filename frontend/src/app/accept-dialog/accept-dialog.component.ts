import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AcceptRequest } from '../model/accept-request.model';

@Component({
  selector: 'app-accept-dialog',
  templateUrl: './accept-dialog.component.html',
  styleUrls: ['./accept-dialog.component.css']
})
export class AcceptDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AcceptDialogComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: AcceptRequest) {
     }

  ngOnInit(): void {
    
  }
  accept(bool:boolean){
    this.dialogRef.close(bool);
  }

}
