import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../service/chatService';
import { User } from '../model/user.model'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router,public service:ChatService) { }

  ngOnInit(): void {
  }
  exit(){
    this.service.deleteCurrentUser();
    this.service.currentUser = new User();
    this.router.navigateByUrl("/sign-in");
  }
}
