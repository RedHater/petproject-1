import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SignInComponent } from './sign-in/sign-in.component';
import { MainComponent } from './main/main.component';
import { ChatComponent } from './chat/chat.component';
import { HeaderComponent } from './header/header.component';
import { AppMaterialModule } from './app-material.module';
import { AuthGuardService } from './service/auth-guard.service';
import { ChatService } from './service/chatService';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AcceptDialogComponent } from './accept-dialog/accept-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    MainComponent,
    ChatComponent,
    HeaderComponent,
    AcceptDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    AppMaterialModule
  ],
  entryComponents: [
    AcceptDialogComponent
  ],
  providers: [AuthGuardService, ChatService],
  bootstrap: [AppComponent]
})
export class AppModule { }
