import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { User } from '../model/user.model';
import { ChatService } from '../service/chatService';

@Component({
  selector:'sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  signInForm: FormGroup;
  horizontalPosition: MatSnackBarHorizontalPosition = 'left';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  constructor( public fb: FormBuilder, private service:ChatService,
     public _snackBar: MatSnackBar, private router: Router) {
    this.signInForm = this.fb.group({
      nickname: ["", [Validators.required]]
    });
  }
  ngOnInit(): void {
  }
  onSubmit() {
    this.service.signIn(this.signInForm.value.nickname).subscribe(ans=>{
      if(ans.message==null){
        this._snackBar.open('Ник занят', '', {
          panelClass:"message_warn",
          duration: 2000,
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
        });
      }else{
        this.service.currentUser.name = this.signInForm.value.nickname;
        this.service.currentUser.id = ans.message;
        console.log(ans);
        console.log(this.service.currentUser);
        this.service.initializeWebSocketConnection();
        this.router.navigateByUrl("/");
      }
    })
  }
}
