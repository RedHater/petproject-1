import { Component, OnDestroy, OnInit  } from '@angular/core';
import { Subscription } from 'rxjs';
import { AcceptRequest, State } from '../model/accept-request.model';
import { User } from '../model/user.model';
import { ChatService } from '../service/chatService';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit,OnDestroy{
  choosenUser;
  private subscriptions: Subscription[] = [];
  userList = [];
  searchList = [];
  search:string;
  constructor(public service:ChatService) { 
    const userSub = this.service.usersSub.subscribe(value=>{
      this.service.getAll().subscribe(
        list=>{
          console.log(list);
          list.splice(list.indexOf(this.service.currentUser.name),1);
          this.userList = list;
          this.searchList = this.userList;
        }
      )
    });
    this.subscriptions.push(userSub);
  }

  ngOnInit(): void {
    this.service.getAll().subscribe(
      list=>{
        console.log(list);
        list.splice(list.indexOf(this.service.currentUser.name),1);
        this.userList = list;
        this.searchList = this.userList;
      })
  }
  ngOnDestroy(){
    this.subscriptions
    .forEach(s => s.unsubscribe());
  }
 
  searchUsers(){
    if(this.search.length == 0){
      this.searchList = this.userList;
    }else{
      let tempArr = [];
      let re = new RegExp(this.search.toLowerCase());
      this.userList.forEach((value:string)=>{
        value.toLowerCase().search(re)!=-1 ? tempArr.push(value):{};
      });
      this.searchList = tempArr;
    }
  }
  
  startChat(user){
    this.choosenUser = user;
    var req = new AcceptRequest();
    this.service.getByName(user).subscribe(rec=>{
      req.sender = this.service.currentUser;
      req.recipient = rec;
      req.state = State.SENT;
      this.service.sendRequest(req);
    })
  }
}
