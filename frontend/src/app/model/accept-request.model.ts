import { User } from "./user.model";

export enum State {
    DECLINED="DECLINED",
    ACCEPTED="ACCEPTED",
    SENT="SENT",
}
export class AcceptRequest{
    sender:User;
    recipient:User;
    state:State;
}