package com.kursach.sms.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class ChatRequest {
    public static enum RequestState {
        ACCEPTED, DECLINED, SENT;
        public static RequestState getById(String id){
            for(RequestState e : values()) {
                if(e.name().equalsIgnoreCase(id)) return e;
            }
            return SENT;
        }

    }
    private User sender;
    private User recipient;
    private RequestState state;
}
