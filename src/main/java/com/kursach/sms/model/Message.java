package com.kursach.sms.model;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Setter
@Getter
public class Message {
    private UUID sender;
    private UUID recipient;
    private String content;
}
