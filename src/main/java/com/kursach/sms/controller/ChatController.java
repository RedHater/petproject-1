package com.kursach.sms.controller;

import com.kursach.sms.model.ChatRequest;
import com.kursach.sms.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class ChatController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @MessageMapping("/chat")
    public void processMessage(@Payload Message message) {
        messagingTemplate.convertAndSendToUser(
                message.getRecipient().toString(),"/messages",
                message);
    }
    @MessageMapping("/req")
    public void sendReqForChat(@Payload ChatRequest request) {
        messagingTemplate.convertAndSendToUser(
                request.getRecipient().getId().toString(),"/chatReqs",
                request);
    }
    @MessageMapping("/accept")
    public void acceptReqForChat(@Payload ChatRequest request) {
            messagingTemplate.convertAndSendToUser(
                    request.getSender().getId().toString(),"/chatReqs",
                    request);

    }
    @MessageMapping("/new")
    public void acceptReqForChat(@Payload String str) {
        messagingTemplate.convertAndSend(
                "/users/", true);
    }
}
