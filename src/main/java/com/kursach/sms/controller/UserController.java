package com.kursach.sms.controller;

import com.kursach.sms.service.UserService;
import com.kursach.sms.model.User;
import com.kursach.sms.sys.conf.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("api/user")
@CrossOrigin(origins = "http://localhost:4200/")
public class UserController {
    @Autowired
    private UserService service;

    @GetMapping("/signIn/{nickName}")
    public ResponseMessage signIn(@PathVariable String nickName){
       return this.service.signIn(nickName);
    }

    @GetMapping("/all/")
    public List<String> getAll(){
        return this.service.getAll();
    }

    @GetMapping("/getByName/{nickName}")
    public User getByName(@PathVariable String nickName){
        return this.service.getByName(nickName);
    }

    @GetMapping("/delete/{id}")
    public void delete(@PathVariable UUID id){
        this.service.delete(id);
    }


}
