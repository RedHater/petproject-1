package com.kursach.sms.service;

import com.kursach.sms.model.User;
import com.kursach.sms.sys.conf.ResponseMessage;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    private Map<UUID,String> users = new HashMap<>();

    public ResponseMessage signIn(String userName){
        if(users.containsValue(userName)){
            return new ResponseMessage(null);
        }else{
            var uuid = UUID.randomUUID();
            users.put(uuid,userName);
            return new ResponseMessage(uuid.toString());
        }
    }
    public List<String> getAll(){
        return new ArrayList<>(users.values());
    }

    public User getByName(String nickName){
        for(UUID key:users.keySet()){
            if(users.get(key).equals(nickName)) return new User(key,nickName);
        }
        return null;
    }
    public User getById(UUID id){
        return new User(id,users.get(id));
    }

    public void delete(UUID id){
        users.remove(id);
    }
}
